package professional.TD.week4.functional.task1;

/*
Создать таймер, который считает время выполнения метода, используя Runnable.
 */
public class Main {
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.measureTime(new SimpleSummator());
        System.out.println(timer.timeNanoSeconds);
        
        Timer timer1 = new Timer();
        timer1.measureTime(new Runnable() {
            @Override
            public void run() {
                long sum = 0;
                for (int i = 1; i <= 1_000_000_000; i++) {
                    sum += i;
                }
                System.out.println(sum);
            }
        });
        System.out.println(timer1.timeNanoSeconds);
        
        Timer timer3 = new Timer();
        timer3.measureTime(() -> {
            long sum = 0;
            for (int i = 1; i <= 1_000_000_000; i++) {
                sum += i;
            }
            System.out.println(sum);
        });
        System.out.println(timer3.timeNanoSeconds);
    }
}
