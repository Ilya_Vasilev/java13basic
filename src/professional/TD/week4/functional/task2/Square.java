package professional.TD.week4.functional.task2;

@FunctionalInterface
public interface Square {
    
    int calculate(int x);
}
