package professional.TD.week4.streams.task3;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordCountLambda {
    public static void main(String[] args) {
        List<String> names = List.of("Sam", "James", "Elena", "James", "Joe", "Sam", "James");
        
        //1 способ
        Set<String> unique = new HashSet<>(names);
        for (String key : unique) {
            System.out.println(key + ": " + Collections.frequency(names, key));
        }
        
        //2 способ
        Map<String, Long> frequencyMap = names.stream()
              .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(frequencyMap);
        /*
        select name, count(*)
        from names
        group by name
        1 строка: "Andrei"
        2 строка: "Andrei"
        ->
        1 строка: "Andrei", 2
         */
        
        //3 способ
        Map<String, Integer> counts = names.parallelStream()
              .collect(Collectors.toConcurrentMap(v -> v, v -> 1, Integer::sum));
        System.out.println(counts);
        
    }
}
