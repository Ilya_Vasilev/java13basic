package professional.TD.week2.task5;

import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;

/*
Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.

Про деревья:
 https://habr.com/ru/post/330644/
 
Структуры данных в картинках:
  https://habr.com/ru/post/128017/
  
  Книга: структуры данных и алгоритмы Java - Лафоре Роберт
 */
public class ConvertHashSet {
    private ConvertHashSet() {
    }
    
    public static <T> TreeSet<T> convertHashSet(HashSet<T> from) {
        TreeSet<T> toReturn = new TreeSet<>();
        //Collections.addAll();
//        toReturn.addAll(from);
//        TreeSet<T> toReturn1 = new TreeSet<>(from);
//        return new TreeSet<>(from);
        for (T elem : from) {
            toReturn.add(elem);
        }
        return toReturn;
    }
    
}
