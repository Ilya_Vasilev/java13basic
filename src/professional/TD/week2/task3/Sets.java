package professional.TD.week2.task3;

import java.util.HashSet;
import java.util.Set;

/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */
public class Sets {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        
        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(3);
        set2.add(2);
        set2.add(2);
        //встроенный метод
        set1.retainAll(set2);
        
        for (Integer elem : set1) {
            System.out.println(elem);
        }
//        set1.forEach(elem -> System.out.println(elem));
//        set1.forEach(System.out::println);
        //метод, который вернет true/false, если есть хоть один элемент
        // в нашей текущей коллекции в другой коллекции
        //Collections.disjoint();
    }
}
