package professional.TD.week2.task2;

public class Pair<T, U> {
    private T first;
    private U second;
    
    public T getFirst() {
        return this.first;
    }
    
    public void setFirst(T value) {
        this.first = value;
    }
    
    public U getSecond() {
        return second;
    }
    
    public void setSecond(U second) {
        this.second = second;
    }
    
    @Override
    public String toString() {
        return "Pair{" +
               "first=" + first +
               ", second=" + second +
               '}';
    }
}
