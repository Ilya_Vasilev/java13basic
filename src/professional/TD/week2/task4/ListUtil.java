package professional.TD.week2.task4;

import java.util.List;

/*
  Реализовать метод, который считает количество элементов в переданном List
  (элемент передается на вход, посчитать количество таких элементов в list)
 */
public class ListUtil {
    private ListUtil() {
    }
    
    public static <T> int countIf(List<T> from, T value) {
        int couter = 0;
        for (T elem : from) {
//            if (elem.equals(value)) {
//                ++couter;
//            }
            if (elem == value) {
                ++couter;
            }
        }
        
        return couter;
    }
}
