package professional.TD.week2.task1;

import java.math.BigDecimal;

/*
Создать класс Pair, который умеет хранить два значения:
a) любого типа (T, U)
b) одинакового типа (T)
c) первый только String, второй только числовой
 */
public class Main {
    public static void main(String[] args) {
//        Pair<String, Integer> pair = new Pair<>();
//        pair.first = "Test first field";
//        pair.second = 123;
//        pair.print();
//
//        Pair<Integer, Double> pair1 = new Pair<>();
//        pair1.first = 345;
//        pair1.second = 3D;
//        pair1.print();

//        Pair<String> pair = new Pair<>();
//        pair.first = "Test first";
//        pair.second = "Test second";
//        pair.print();
        
        Pair<String, BigDecimal> pair = new Pair<>();
        pair.first = "123";
        pair.second = new BigDecimal(123);
        pair.print();
        
    }
}
