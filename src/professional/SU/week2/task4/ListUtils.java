package professional.SU.week2.task4;

import java.util.List;

public class ListUtils {
    
    private ListUtils() {
    }
    
    public static <T> int countIf(List<T> elements, T element) {
        int counter = 0;
        for (T elem : elements) {
            //сравнение по ЗНАЧЕНИЮ
//            if (elem.equals(element)) {
//                ++counter;
//            }
            
            //сравнение по ССЫЛКЕ!!!!!!
            if (elem == element) {
                ++counter;
            }
        }
        return counter;
    }
}
