package professional.SU.week2.task3;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/*
 На вход подаются два сета, вывести уникальные элементы,
 которые встречаются и в первом и во втором.
 */
public class Sets {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>();
        set1.add(0);
        set1.add(0);
        set1.add(1);
        set1.add(2);
        set1.add(3);
        //     System.out.println(set1);
        
        Set<Integer> set2 = new HashSet<>();
        set2.add(4);
        set2.add(5);
        set2.add(3);
        set2.add(2);
        set2.add(2);
        
        set1.retainAll(set2);
        
        for (Integer element : set1) {
            System.out.println(element);
        }
//        set1.forEach(elem -> System.out.println(elem));
//        set1.forEach(System.out::println);
        //метод true/false есть ли хоть один элемент нашей текущей коллекции в другой коллекции
        //Collections.disjoint()
    }
    
}
