package professional.SU.week2.task7;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/*
Простая задача: сохранить в мапе три элемента (1, “first”).
Вывести элемент значение по ключу 2
 */
public class MapSave {
    public static void main(String[] args) {
        Map<Integer, String> hashMap = new HashMap<>();
        
        hashMap.put(1, "first");
        hashMap.put(2, "second");
        hashMap.put(3, "third");
//        hashMap.put(2, "fourth");

//        Map<Integer, List<String>>;
//        ConcurrentHashMap<String, Map<Integer, Object>>
        System.out.println("MAP ELEMENTS: " + hashMap);
        System.out.println("значение по ключу 2: " + hashMap.get(2));
    }
}
