package professional.SU.week3.reflection.modifiers;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/*
Получение модификаторов класса
Метод getModifiers() позволяет узнать все модификаторы, с которыми был объявлен класс:
public native int getModifiers();
Модификаторы пакуются внутри битов инта, распаковать информацию можно методами класса Modifier:
public static boolean isPublic(int mod) { }
public static boolean isPrivate(int mod) { }
public static boolean isProtected(int mod) { }
public static boolean isStatic(int mod) { }
public static boolean isFinal(int mod) { }
public static boolean isInterface(int mod) { }
public static boolean isAbstract(int mod) { }

Работа с полями
Метод getFields() возвращает все публичные поля класса или интерфейса, включая унаследованные:
public Field[] getFields() throws SecurityException

Метод getDeclaredFields() возвращает вообще все поля класса или интерфейса, но исключая унаследованные:
public Field[] getDeclaredFields() throws SecurityException

Есть еще методы getField() и getDeclaredField() позволяют найти поле по его строчному имени.

Основные методы класса Field:
get() и set() позволяют прочитать и записать значение поля:
getName() возвращает имя поля
getType() возвращает объект Class для его типа
getModifiers() тоже есть и работает так же, как для Class
 */
public class ReflectionClassModifiers {
    public static void main(String[] args) throws IllegalAccessException {
//        System.out.println(Modifier.isPublic(ReflectionClassModifiers.class.getModifiers()));
        //задача 2
//        printAllFields(Task2.class);
        //Задача 3
        Task2 task2 = new Task2();
        task2.i = 987;
        printAllFieldsWithValues(Task2.class, task2);
    }
    
    //Задача 2
    //вывести все поля класса, их модификаторы и типы.
    public static void printAllFields(Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) {
                System.out.println("public ");
            }
            if (Modifier.isProtected(mods)) {
                System.out.println("protected ");
            }
            if (Modifier.isPrivate(mods)) {
                System.out.println("private ");
            }
            if (Modifier.isStatic(mods)) {
                System.out.println("static ");
            }
            if (Modifier.isFinal(mods)) {
                System.out.println("final ");
            }
            System.out.println(field.getType().getName());
        }
    }
    
    //Задача 3
    //Продолжение задачи 2.
    // Создать инстанс класса Task вывести значения его полей.
    private static void printAllFieldsWithValues(Class<?> clazz, Task2 task2) throws IllegalAccessException {
        printAllFields(clazz);
        System.out.println("\n");
        for (Field field : clazz.getDeclaredFields()) {
            int mod = field.getModifiers();
            if (Modifier.isPrivate(mod)) {
                field.setAccessible(true);
            }
            System.out.println(field.get(task2));
        }
    }
}
