package professional.SU.week3.reflection;

/*
Определение
Рефлексия (англ. reflection) – знание кода о самом себе
К рефлексии можно отнести возможность проитерироваться по всем полям класса
или найти и создать объект класса, по имени, заданному через текстовую строку

Class – основной класс для рефлексии в Java

 */
public class Reflection {
    /*
        способы получения объекта Class
         */
    public static void main(String[] args) {
        //1 способ
        //псевдополе .class
        Class<String> c1 = String.class;
        //2 способ
        //метод getClass() у Object
        //Этот метод учитывает полиморфизм и возвращает реальный класс, которым является объект
        CharSequence sequence = "My String";
        Class<? extends CharSequence> c2 = sequence.getClass();// вернётся Class<String>
        System.out.println(c2);
        //3 способ
        //найти этот класс по строчному имени
        try {
            Class<?> integerClass = Class.forName("java.lang.Integer");
        }
        catch (ClassNotFoundException e) {
            System.out.println("не могу найти такой класс");
        }
    
    }
}
