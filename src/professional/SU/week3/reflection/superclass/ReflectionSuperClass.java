package professional.SU.week3.reflection.superclass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Получение родителей
Метод getSuperclass() возвращает Class родителя текущего класса
Метод getInterfaces() возвращает список Class’ов интерфейсов,
реализуемых текущим классом.
 */
public class ReflectionSuperClass {
    public static void main(String[] args) {
        //вывести для класса D все интерфейсы
//        for (Class<?> cls : D.class.getInterfaces()) {
//            System.out.println(cls.getName());
//        }
        
        //задача 1
        List<Class<?>> result = getAllInterfaces(D.class);
        for (Class<?> cls : result) {
            System.out.println(cls.getName());
        }
    }
    
    /*
    Задача 1
    Получить все интерфейсы класса, включая интерфейсы от классов-родителей.
     */
    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            interfaces.addAll(Arrays.asList(cls.getInterfaces()));
            cls = cls.getSuperclass();
        }
        return interfaces;
    }
    
    
}
