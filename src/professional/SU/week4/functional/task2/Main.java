package professional.SU.week4.functional.task2;

/*
Задача 2
С помощью функционального интерфейса выполнить подсчет квадрата числа
 */
public class Main {
    public static void main(String[] args) {
        Square s = new Square() {
            @Override
            public int calculateSquare(int x) {
                return x * x;
            }
        };
        
        System.out.println(s.calculateSquare(3));
        
        Square s1 = (int x) -> x * x;
        System.out.println(s1.calculateSquare(3));
    }
}
