package professional.SU.week4.functional.task3;

@FunctionalInterface
public interface MyInterface {
    double getPiValue();
}
