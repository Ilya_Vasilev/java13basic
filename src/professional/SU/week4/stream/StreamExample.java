package professional.SU.week4.stream;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class StreamExample {
    public static void main(String[] args) {
        List<String> myPlaces = List.of("Nepal, Kathmandu", "Nepal, Pokhara", "India, Delhi", "USA, New York", "Africa, Nigeria");
        
        myPlaces.stream()
              .filter((place) -> place.startsWith("Nepal"))
//              .map((place) -> place.toUpperCase())
              .map(String::toUpperCase)
              .sorted()
//              .forEach((place) -> System.out.println(place)
              .forEach(System.out::println);
        //Выражение System.out::println является ссылкой на метод, который эквивалентен лямбда-выражению x -> System.out.println(x).
        
    }
}
