package professional.TU.week1.exceptions.task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/*
Дан код, необходимо корректно обработать исключения и ресурсы
public static void main(String[] args) {
        Scanner scanner = new Scanner(new File("input.txt"));
        ArrayList<String> lines = new ArrayList<>();
        while (scanner.hasNextLine()) {
            lines.add(scanner.nextLine());
        }
        
        for (String line : lines) {
            System.out.println(line);
        }
    }
 */
public class FileReader {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(new File("input.txt"))) {
            ArrayList<String> lines = new ArrayList<>();
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }
            for (String line : lines) {
                System.out.println(line);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("FileReader#main!error: " + e.getMessage());
        }
    }
    
    
    public void example1(){
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("input.txt"));
            ArrayList<String> lines = new ArrayList<>();
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }
            for (String line : lines) {
                System.out.println(line);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("FileReader#main!error: " + e.getMessage());
        }
        finally {
            if (scanner != null) {
                scanner.close();
            }
        }
    }
    
}
