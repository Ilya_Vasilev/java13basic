package professional.TU.week4.streams.task2;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/*
Посчитать n! с помощью стримов
 */
public class Factorial {
    public static void main(String[] args) {
        System.out.println(fact(5));
    
        System.out.println(fact2(30));
    }
    
    public static int fact(int n) {
        return IntStream
              .rangeClosed(1, n)
              .reduce(1, (int x, int y) -> x * y);
    }
    
    public static BigInteger fact1(int n) {
        return IntStream.rangeClosed(1, n)
              .mapToObj(BigInteger::valueOf)
              .reduce(BigInteger.ONE, BigInteger::multiply);
    }
    
    public static BigInteger fact2(int n) {
        return Stream.iterate(BigInteger.ONE, i -> i.add(BigInteger.ONE))
              .limit(n)
              .reduce(BigInteger.ONE, BigInteger::multiply);
    }
}
