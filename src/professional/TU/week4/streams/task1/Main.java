package professional.TU.week4.streams.task1;


import professional.TU.week4.functional.task2.Square;

import java.util.ArrayList;
import java.util.List;

/*
Использовать реализованный функциональный интерфейс Square на массиве чисел, вывести на экран
 */
public class Main {
    public static void main(String[] args) {
        Square square = (int x) -> x * x;
        List<Integer> nums = new ArrayList<>();
        nums.add(3);
        nums.add(10);
        
        nums.stream()
//              .map(num -> square.calculate(num))
              .map(square::calculate)
              .forEach(System.out::println);
    }
}
