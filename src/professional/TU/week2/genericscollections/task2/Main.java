package professional.TU.week2.genericscollections.task2;

public class Main {
    public static void main(String[] args) {
        Pair<String, Integer> pair = new Pair<>();
        pair.setFirst("testFirstValue");
        pair.setSecond(Integer.parseInt("123"));
        System.out.println(pair);
    }
}
