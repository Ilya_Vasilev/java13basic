package professional.TU.week2.genericscollections.task5;

import java.util.HashSet;
import java.util.TreeSet;

/*
Создать метод, переводящий из HashSet в TreeSet. Вывести оба варианта.

Про деревья:
 https://habr.com/ru/post/330644/
 
Структуры данных в картинках:
  https://habr.com/ru/post/128017/
 */
public class ConvertHashSet {
    
    public static <T> TreeSet<T> convertHashSet(HashSet<T> from) {
        TreeSet<T> toReturn = new TreeSet<>();
        //toReturn.addAll(from);
        for (T elem : from) {
//            Collections.addAll(toReturn, elem);
            toReturn.add(elem);
        }
        return toReturn;
    }
    
    private ConvertHashSet() {
    
    }
}
