package professional.TU.week2.genericscollections.task4;

import java.util.List;

/*
  Реализовать метод, который считает количество элементов в переданном List
  (элемент передается на вход, посчитать количество таких элементов в list)
 */
public class ListUtils {
    
    public static <T> int countIf(List<T> from, T elem) {
        int counter = 0;
        for (T element : from) {
            if (element.equals(elem)) {
                ++counter;
            }
//            if (element == elem) {
//                ++counter;
//            }
        }
        return counter;
    }
    
    private ListUtils() {
    
    }
}
