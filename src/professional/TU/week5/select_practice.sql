--INSERT INTO books(title, author, date_added) VALUES ('Недоросль', 'Д. И. Фонвизин', now());
--INSERT INTO books(title, author, date_added) VALUES ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
INSERT INTO books(title, author, date_added) VALUES ('Доктор Живаго', 'Б. Л. Пастернак', now() - interval '24h');
INSERT INTO books(title, author, date_added) VALUES ('Сестра моя - жизнь', 'Б. Л. Пастернак', now());


--1. Достать запись под id = 2
select *
from books
where id = 2;

--ограничение на выходе по количеству
select *
from books
order by date_added desc
limit 3
--rownum - oracle
--top - sqlServer

--2. Найти автора книги по названию ‘Недоросль’
select *
from books
where title = 'Недоросль';

--3. Найти все книги Пастернака
select *
from books
where lower(trim(author)) like lower('%Пастернак');

--4. Вывести максимальный id в таблице
select max(id) from books;
select count(*) from books;
--select avg(id) from books;
--avg, min, sum, count, mod, power....

--5. Количество каждой книги вывести
select author, title, count(*)
from books
group by author, title;

--6. Найти все книги Радищева или Пастернака отсортированные по дате в обратном порядке
select *
from books
where lower(trim(author)) like lower('%Пастернак') or upper(author) like upper('%Радищев%')
order by date_added desc;

--7. Найти все книги Пастернака добавленные вчера
select *
from books
where lower(trim(author)) like lower('%Пастернак') and date_added <= now() - interval '24h';