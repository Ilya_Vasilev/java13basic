/*
Создать таблицу книг со следующими полями
- id ключ
- title- наименование книги
- author - автор книги
- date_added - дата добавления книги
 */
--DDL (Data Definition Language)
create table books
(
    id         serial primary key,
    title      varchar(30) not null,
    author     varchar(50) not null,
    date_added timestamp   not null
);
--посмотреть данные таблицы (сначала пусто)
select *
from books;

--insert data
--DML (Data Manipulation Language)
insert into books--(title, author, date_added)
values (nextval('books_id_seq'), 'Недоросль', 'Д. И. Фонвизин', now());

insert into books(title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now());

--команда изменения структуры таблицы
alter table books
alter column title type varchar(100);

insert into books(title, author, date_added)
values ('Путешествие из Петербурга в Москву', 'А. Н. Радищев', now() - interval '24h');
commit;

rollback;

select *
from books;




