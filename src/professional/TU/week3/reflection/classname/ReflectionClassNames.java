package professional.TU.week3.reflection.classname;

import java.util.AbstractMap;
import java.util.HashMap;

/*
получение имени класса из Class

- getName() - полное имя класса (java.lang.Integer)
- getSimpleName() - короткое имя класса, без указания пакета (пакетов) (Integer)
- getPackage().getName() - получить только имя пакета (java.lang)
 */
public class ReflectionClassNames {
    public static void main(String[] args) {
//        printNamesForClass(int.class, "int.class (primitives)");
        printNamesForClass(String.class, "String.class");
        printNamesForClass(java.util.HashMap.SimpleEntry.class,
                           "java.util.HashMap.SimpleEntry.class (nested class)");
        printNamesForClass(new java.io.Serializable() {}.getClass(),
                           "new java.io.Serializable() {}.getClass() (анонимный вложенный класс)");
    }
    
    public static void printNamesForClass(Class<?> clazz, String label) {
        System.out.println(label + ":");
        System.out.println(" getName(): " + clazz.getName());
        System.out.println(" getSimpleName(): " + clazz.getSimpleName());
        System.out.println(" getPackage().getName(): " + clazz.getPackage().getName());
        System.out.println();
    }
}
