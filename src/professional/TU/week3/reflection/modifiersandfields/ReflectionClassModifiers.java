package professional.TU.week3.reflection.modifiersandfields;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/*
Получение модификаторов класса
getModifiers() - позволяет узнать все модификаторы, с которыми был объявлен класс
public native int getModifiers();
Модификаторы пакуются внутри битов инта,
распаковать информацию можно методами класса Modifier:
public static boolean isPublic(int mod) { }
public static boolean isPrivate(int mod) { }
public static boolean isProtected(int mod) { }
public static boolean isStatic(int mod) { }
public static boolean isFinal(int mod) { }
public static boolean isInterface(int mod) { }
public static boolean isAbstract(int mod) { }
 */
/*
Работа с полями
 - getFields() - возвращает все публичные поля класса или интерфейса, включая унаследованные:
public Field[] getFields() throws SecurityException;
SecurityException бросается в случае запрета доступа к пакету, в котором лежит класс

 - getDeclaredFields() возвращает вообще все поля класса или интерфейса, но исключая унаследованные:
 public Field[] getDeclaredFields() throws SecurityException;
 
 - getField(), getDeclaredField() - поле по его строчному имени.
 
  Основные методы класса Field:
    get() и set() позволяют прочитать и записать значение поля
    getName() возвращает имя поля
    getType() возвращает объект Class для его типа
    getModifiers() тоже есть и работает так же, как для Class
 */
public class ReflectionClassModifiers {
    
    public static void main(String[] args) throws IllegalAccessException {
        //example:
//        System.out.println(Modifier.isPublic(ReflectionClassModifiers.class.getModifiers()));
        
        //Задача 2:
        //printAllClassFields(Task2.class);
        
        //Продолжение задачи 2:
        //Для приватных полей нужно делать setAccessible = true
        Task2 task2 = new Task2();
        task2.i = 777;
        printAllClassFieldsWithValues(Task2.class, task2);
    }
    
    //Продолжение задачи 2. Создать инстанс класса Task вывести значения его полей.
    private static void printAllClassFieldsWithValues(Class<Task2> task2Class, Task2 task2) throws IllegalAccessException {
        printAllClassFields(task2Class);
        System.out.println("Значения:");
        for (Field field : task2Class.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPrivate(mods)) {
                field.setAccessible(true);
            }
            System.out.println(field.get(task2));
            
        }
    }
    
    //Задача 2
    //Вывести все поля класса, их модификаторы и типы.
    public static void printAllClassFields(Class<?> clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            int mods = field.getModifiers();
            if (Modifier.isPublic(mods)) {
                System.out.println("public ");
            }
            if (Modifier.isProtected(mods)) {
                System.out.println("protected ");
            }
            if (Modifier.isPrivate(mods)) {
                System.out.println("private ");
            }
            if (Modifier.isStatic(mods)) {
                System.out.println("static ");
            }
            if (Modifier.isFinal(mods)) {
                System.out.println("final ");
            }
            System.out.println(field.getType().getName() + " ");
        }
    }
}
