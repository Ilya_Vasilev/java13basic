package professional.TU.week3.reflection.methods;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
Работа с методами
getMethods(),
getDeclaredMethods(),
getMethod(),
getDeclaredMethod()

Основные методы класса Method:
getModifiers()
getName()
newInstance() - Создаёт новый инстанс класса, вызвав конструктор

Работа с конструкторами:
getConstructors()
getDeclaredConstructors()
getConstructor()
getDeclaredConstructor()
 */
public class ReflectionClassMethods {
    //Задача 4:
    //Сконструировать класс. То есть вызвать конструктор класса и обработать все возможные ошибки.
    public static void main(String[] args) {
        Class<Task4> cls = Task4.class;
        try {
            Constructor<Task4> constructor = cls.getDeclaredConstructor(int.class, String.class);
            Task4 result = constructor.newInstance(142, "Test Constructor");
            System.out.println(result);
            System.out.println(result.a);
            System.out.println(result.b);
        }
        catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            System.out.println("ERROR: " + e.getMessage());
        }
    }
}
