package professional.TU.week3.annotations;

/*
Обращение к аннотации с помощью Reflection
Для проверки наличия аннотации у класса, существуют методы класса Class:
isAnnotationPresent()
Проверяет, есть ли у класса данная аннотация
getAnnotation()
Возвращает аннотацию, если она есть у класса, иначе null
getAnnotations()
Возвращает все аннотации, примененные к классу
getDeclaredAnnotations()
Как предыдущий, но не учитывает аннотации, унаследованные с помощью @Inherited

Задача 6
Создать класс, который по аннотации
@ClassDescription из предыдущего задания выводит все описание на экран:
Так как по дефолту аннотации создаются с retentionPolicy = class,
то для того, чтобы мы могли рефлексией обратиться к ней,
нужно добавить к классу аннотации @ClassDescription следующую строчку
@Retention(RetentionPolicy.RUNTIME)
 */
public class AnnotationReflection {
    
    public static void main(String[] args) {
        writeDescription(MyPerfectClass.class);
    }
    
    public static void writeDescription(Class<?> clazz) {
        if (!clazz.isAnnotationPresent(ClassDescription.class)) {
            return;
        }
        ClassDescription classDescription = clazz.getAnnotation(ClassDescription.class);
        System.out.println("Автор: " + classDescription.author());
        System.out.println("Дата: " + classDescription.date());
        System.out.println("Версия: " + classDescription.currentRevision());
        System.out.println("Проверяющие: ");
        for (String str : classDescription.reviewers()) {
            System.out.println(">" + str);
        }
    }
}
