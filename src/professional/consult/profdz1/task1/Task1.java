package professional.consult.profdz1.task1;

import java.io.BufferedReader;
import java.io.UncheckedIOException;

/*
Создать собственное исключение MyCheckedException, являющееся проверяемым.
 */
public class Task1 {
    public static void main(String[] args) {
        try {
            methodZero(1);
            methodZero(0);
        }
        catch (MyCheckedException e) {
            System.out.println(e.getMessage());
        }
        finally {
        }
    }
    
    public static void methodZero(int i) throws MyCheckedException {
        if (i == 0) {
            System.out.println("0");
        }
        else {
            throw new MyCheckedException("this is not a zero");
        }
    }
}
