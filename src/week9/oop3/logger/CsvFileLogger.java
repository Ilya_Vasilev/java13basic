package week9.oop3.logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import static week9.oop3.logger.FileExtensions.CSV_FILE_EXTENSION;

public class CsvFileLogger
      extends FileNameHandler
      implements Logger {

//    private final String cvsExtension = ".csv";
    
    private final String fileName;
    
    public CsvFileLogger(String fileName) {
        this.fileName = fileName;
    }
    
    public CsvFileLogger() {
        this.fileName = getDefaultFileName();
    }
    
    
    @Override
    public void log(String message) {
        try (Writer writer = new FileWriter(fileName + getExtension(), true)) {
            writer.write(message + "\n");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public String getExtension() {
        return CSV_FILE_EXTENSION;
    }
}
