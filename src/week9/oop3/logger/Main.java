package week9.oop3.logger;

public class Main {
    public static void main(String[] args) {
        ConsoleLogger consoleLogger = new ConsoleLogger();
        consoleLogger.log("мы в main");
        
        Logger txtLogger = new TxtFileLogger();
        Bulb bulb = new Bulb(txtLogger);
        
        bulb.turnOn();
        bulb.turnOff();
        bulb.isShining();
        
        Logger csvLogger = new CsvFileLogger("лог_лампочки");
        Bulb bulb1 = new Bulb(csvLogger);
        //Bulb bulb1 = new Bulb(consoleLogger);
    
        bulb1.turnOn();
        bulb1.turnOff();
        bulb1.isShining();
    }
}
