package week10.sort;

import java.util.Arrays;
import java.util.Random;

public class TestSort {
    public static void main(String[] args) {
        int n = 20000;
        int[] array = new int[n];
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            array[i] = r.nextInt(100000);
        }
        
        int[] arrayToSort = Arrays.copyOf(array, array.length);
        CompareSort.javaSort(arrayToSort);
        
        arrayToSort = Arrays.copyOf(array, array.length);
        CompareSort.bubbleSort(arrayToSort);
        
        arrayToSort = Arrays.copyOf(array, array.length);
        CompareSort.bubbleSort2(arrayToSort);
        
        arrayToSort = Arrays.copyOf(array, array.length);
        CompareSort.insertionSort(arrayToSort);
        
        arrayToSort = Arrays.copyOf(array, array.length);
        CompareSort.selectionSort(arrayToSort);
        
        arrayToSort = Arrays.copyOf(array, array.length);
        CompareSort.radixSort(arrayToSort);
    }
    /*
0.004629125
0.373489625
0.475575917
0.025523417
0.101066958
0.001424334
     */
}
