package week10.sort;

import java.util.Arrays;

//https://www.sortvisualizer.com/
//https://www.youtube.com/watch?v=semGJAJ7i74&list=PLOmdoKois7_FK-ySGwHBkltzB11snW7KQ&index=3
public class CompareSort {
    
    /*
    Использование метода сортировки встроенной в Java.
    Быстрая сортировка (quicksort)
    Обычный quicksort делит массив на два отрезка, выбрав случайный элемент P.
    Потом сортирует массив так, чтобы все элементы меньше P попали в первый отрезок, а остальные — во второй.
    Затем алгоритм рекурсивно повторяется на первом и на втором отрезках.
    
    Dual-pivot quicksort делит массив на три отрезка, вместо двух.
    В результате количество операций перемещения элементов массива существенно сокращается.
     */
    public static void javaSort(int[] array) {
        long startTime = System.nanoTime();
        Arrays.sort(array);
        long stopTime = System.nanoTime();
        System.out.println((double) (stopTime - startTime) / 1_000_000_000);
    }
    
    /*
    Сортировка пузырьком O(N^2)
    Сортировка пузырьком заключается в следующем:
начиная с начала массива просматриваем попарно по 2 элемента
(первый со вторым, второй с третим, третий с четвертым и т.д.).
Если второй элемент в паре меньше первого элемента – перемещаем его на место первого, а первый на место второго.
Это мы делаем для всех элементов.
После того, как мы дошли до конца массива (сравнили предпоследний и последний элементы и сделали обмен, если нужно),
проверяем, был ли хотя бы один обмен. Если да, значит массив не отсортирован и начинаем все сначала.
Повторяем такие проходы, пока не будет так, что мы проверили попарно все элементы от начала до конца, а обмена ни одного не было.
Таким образом элементы с самыми маленькими значениями потихоньку перемещаются справа налево.
То есть они как будто всплывают, как мыльный пузырь. Отсюда и название метода – пузырьком.
https://vertex-academy.com/tutorials/wp-content/uploads/2018/01/vertex-bubl-sort-2.gif
     */
    public static void bubbleSort(int[] array) {
        long startTime = System.nanoTime();
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j - 1] > array[j]) {
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        long stopTime = System.nanoTime();
        System.out.println((double) (stopTime - startTime) / 1_000_000_000);
    }
    
    public static void bubbleSort2(int[] array) {
        long startTime = System.nanoTime();
        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int j = 1; j < array.length; j++) {
                if (array[j - 1] > array[j]) {
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                    isSorted = false;
                }
            }
        }
        long stopTime = System.nanoTime();
        System.out.println((double) (stopTime - startTime) / 1_000_000_000);
    }
    
    /*
    Сортировка вставками (Insertion Sort)
    Сортировка вставками O(N^2)
    Суть его заключается в том что, на каждом шаге алгоритма мы берем один из элементов массива,
    находим позицию для вставки и вставляем.
    Стоит отметить что массив из 1-го элемента считается отсортированным.
    https://hsto.org/getpro/habr/post_images/7c6/f3f/788/7c6f3f788a2defc60bcfb13407c01abd.gif
     */
    public static void insertionSort(int[] array) {
        long startTime = System.nanoTime();
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i - 1;
            //int j = i;
//            while (j > 0 && array[j - 1] > current) {
//                array[j] = array[j - 1];
//                j--;
//            }
//            array[j] = current;
            while (j >= 0 && current < array[j]) {
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = current;
        }
        long stopTime = System.nanoTime();
        System.out.println((double) (stopTime - startTime) / 1_000_000_000);
    }
    
    /*
   Сортировка выбором - O(N^2)
   В неотсортированном подмассиве ищется локальный максимум (минимум).
   Найденный максимум (минимум) меняется местами с последним (первым) элементом в подмассиве.
   Если в массиве остались неотсортированные подмассивы — смотри пункт 1.
   https://hsto.org/webt/yt/cs/fz/ytcsfzyhzn9xy8opfyodmgz-a4u.gif
    */
    public static void selectionSort(int[] array) {
        long startTime = System.nanoTime();
        
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int minId = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    minId = j;
                }
            }
            int temp = array[i];
            array[i] = min;
            array[minId] = temp;
        }
        
        long stopTime = System.nanoTime();
        System.out.println((double) (stopTime - startTime) / 1_000_000_000);
    }
    //!!! доп память O(max)
    public static void radixSort(int[] array) {
        long startTime = System.nanoTime();
        
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }
        
        int[] couters = new int[max + 1];
        for (int i = 0; i < array.length; i++) {
            couters[array[i]] += 1;
        }
        
        int idx = 0;
        for (int i = 0; i < couters.length; i++) {
            for (int j = 0; j < couters[i]; j++) {
                array[idx] = i;
                idx++;
            }
        }
    
        long stopTime = System.nanoTime();
        System.out.println((double) (stopTime - startTime) / 1_000_000_000);
    }
}
