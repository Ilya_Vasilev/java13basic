package week10;

import week10.xmltags.XmlFirstTag;

public interface IXmlFileStructure {
    
    XmlFirstTag fillFirstTag();
    
    void fillSecondTag();
    
    void fillNTag();
}
